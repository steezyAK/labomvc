﻿using laboMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace laboMVC.Controllers
{
    public class IndexController : Controller
    {

        [HttpPost] // Ajoutez cette annotation pour indiquer que cette action répond aux requêtes HTTP POST
        public ActionResult ChangerLangue(string langue)
        {
            // Mettre à jour la langue dans la session ou le cookie
            Session["Langue"] = langue; // Vous pouvez utiliser une autre méthode pour stocker la langue

            // Rediriger vers la page précédente ou une page spécifique dans la nouvelle langue
            return RedirectToAction("Index");
        }

        // GET: Index
        public ActionResult Index(string langue)
        {
            langue = Session["Langue"] as string ?? "Fr"; // Par défaut, la langue est le français


            if (langue == "Fr")
            {
                List<string> Mots;
                // Récupérer les données en français depuis la base de données
                using (Epicerie db = new Epicerie())
                {
                    Mots = db.Langues.Select(f => f.Francais).ToList();
                }
                ViewBag.Mots = Mots; // Passer les données à la vue
            }
            else if (langue == "Ang")
            {
                List<string> Mots;
                // Récupérer les données en français depuis la base de données
                using (Epicerie db = new Epicerie())
                {
                    Mots = db.Langues.Select(f => f.Anglais).ToList();
                }
                ViewBag.Mots = Mots;
            }




            using (Epicerie db = new Epicerie())
            {
                //db.Langues.Add(new Langue { Choix = "Francais" });
                //db.Langues.Add(new Langue { Choix = "Anglais" });
                //db.Langues.Add(new Langue { Choix = "Arabe" });
                db.Langues.Count();
                db.SaveChanges();
            }
            return View(new User { });
        }

        [HttpPost]
        public ActionResult Index(User users)
        {
            if (IsMember(users))
            {
                return RedirectToAction("Index", "User");
            }
            else
            {
                TempData["error"] = true;
                return RedirectToAction("Index");
            }
        }

        // les methodes de verifiactions
        public bool IsMember(User u)
        {
            Boolean status = false;
            using (Epicerie db = new Epicerie())
            {
                var users = db.Users.Where(user => user.Nom == u.Nom && user.Password == u.Password).ToList();
                if (users.Count() > 0)
                {
                    Session["NomUtilisateur"] = users.First().Nom;
                    status = true;
                }
            }
            return status;
        }

    }
}