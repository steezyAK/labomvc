﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace laboMVC.Models
{
    public class Langue
    {
        public int Id { get; set; }
        public string Francais { get; set; }
        public string Anglais { get; set; }
        public string Arabe { get; set; }
    }
}