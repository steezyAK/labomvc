﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace laboMVC.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public decimal Prix { get; set; }
        public string Description { get; set; }

        public int CategorieId { get; set; }
        public virtual Categorie Categorie { get; set; }
    }
}