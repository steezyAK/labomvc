﻿using laboMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace laboMVC.Models
{
    public class Epicerie : DbContext
    {
        public Epicerie()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LaboAspMvc.Models.Epicerie, LaboAspMvc.Migrations.Configuration>());
        }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Categorie> Categories { get; set; }




        public DbSet<Langue> Langues { get; set; }

    }
}