﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace laboMVC.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Nom { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        //role admin ou utilisateur normale
        public bool IdRole { get; set; }
        public virtual Role Roles { get; set; }



    }