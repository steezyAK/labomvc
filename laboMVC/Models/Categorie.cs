﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace laboMVC.Models
{
    public class Categorie
    {
        [Key]
        public int CategorieId { get; set; }
        public string Name { get; set; }
        public virtual List<Article> Articles { get; set; }
    }
}